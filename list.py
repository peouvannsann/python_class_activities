import copy
import time

def bubble_sort(numbers_to_sort):

  please_loop = True
  sorted_list = copy.deepcopy(numbers_to_sort)

  # sorted_list = [-2, 4, 8, 12, 29, 36, -17, 45, 2, 12, 12, 3, 3, -52, 78]
  

  while please_loop:

    please_loop = False

    for i in range(1, len(sorted_list)):

      if sorted_list[i] < sorted_list[i - 1]:

        please_loop = True

        # sorted_list[i], sorted_list[i-1] = sorted_list[i-1], sorted_list[i]

        temp = sorted_list[i] # => 12
        sorted_list[i] = sorted_list[i-1] # sorted_list[i] => -2
        sorted_list[i-1] = temp # sorted_list[i-1] => 12
  return sorted_list


def insertion_sort(numbers_to_sort):

  sorted_list = copy.deepcopy(numbers_to_sort)

  for i in range(1, len(sorted_list)):

    key = sorted_list[i]

    j = i-1

    while j >= 0 and key < sorted_list[j]:

      sorted_list[j + 1] = sorted_list[j]
      j -= 1
      
    sorted_list[j + 1] = key

  return sorted_list



def selection_sort(numbers_to_sort):

  sorted_list = copy.deepcopy(numbers_to_sort)

  num = range(len(sorted_list))

  for i in num:
    
    mininum = i 

    for j in range(i+1, len(sorted_list)):

      if sorted_list[j] < sorted_list[mininum]:
        mininum = j
    
    temp = sorted_list[i]
    sorted_list[i] = sorted_list[mininum]
    sorted_list[mininum] = temp
  
  return sorted_list


# list is not sorted
numbers = [12, -2, 4, 8, 29, 45, 78, 36, -17, 2, 12, 12, 3, 3, -52]


# print the smallest number
a = bubble_sort(numbers)
print('The smallest number is ', *a[:1])


# print the biggest number
print('The biggest number is ', a[-1])


# print all even numbers
even_list = []
for i in range(len(a)):
  if a[i] % 2 == 0:
    even_list.append(a[i])
print('EVEN NUMBER : ', even_list)


# how often does the first number appear in the list?
count = 0
for i in range(len(a)):
  if numbers[i] == numbers[0]:
    count += 1
print('THE FIRST NUMBER APPEAR IN THE LIST ARE : ',count,'times')


# print the average
average = sum(a) / len(a)
print('AVERAGE IS : ' + str(round(average,2)))


# print the sum of all the negative numbers
sum_negative = 0
for i in range(len(a)):
  if a[i] < 0:
    sum_negative +=a[i]
print('SUM OF ALL THE NEGATIVE NUMBER IS',sum_negative)


# sort = display the list this way:
# [-52, -17, -2, 2, 3, 3, 4, 8, 12, 12, 12, 29, 36, 45, 78]
print('\n')


# sort the list using bubble sort
start = time.time()
print(a)
end = time.time()
done = end - start
print('DURATION BUBBLE SORT :',done)


# sort the list using insertion sort
start = time.time()
selection_sort(numbers)
print(insertion_sort(numbers))
end = time.time()
done = end - start
print('DURATION INSERTION SORTED: ',done)


# SORTED the list using selection SORTED
start = time.time()
selection_sort(numbers)
print(selection_sort(numbers))
end = time.time()
done = end -start 
print('DURATION SELECTION SORTEDED: ',done)

